/*	--*- c -*--
 * Copyright (C) 2018 Enrico Scholz <enrico.scholz@ensc.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <sysexits.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>

#include "../ensc-lib/list.h"
#include "../ensc-lib/sd-notify.h"

#include <linux/netlink.h>
#include <linux/rtnetlink.h>

struct ipchange_request {
	struct list_head	head;
	unsigned int		seq;
	size_t			len;
	unsigned char		buf[];
};

#define CHG_DFLT_MODE		CHG_RM_TMP

struct reqinfo {
	enum {
		/* set preferred lt to zero to mark address as 'deprecated' */
		CHG_LT,
		/* removed 'IFA_F_MANAGETEMPADDR' from site-local addresses
		 *
		 * TODO: *not* working; kernel disables 'use_temp' after some
		 * time */
		CHG_RM_TMP,
	}	type;

	struct in6_addr		addr;
	uint32_t		flags;
	struct ifa_cacheinfo	cache_info;
	unsigned int		scope;
};

static int send_request(int fd, struct ipchange_request *req)
{
	ssize_t			l;

	l = send(fd, req->buf, req->len, 0);
	if (l < 0) {
		fprintf(stderr, "send(<NETLINK-CHANGE>): %s", strerror(errno));
		return -1;
	}

	return 0;
}

static int request_netlink_info(int fd)
{
	struct {
		struct nlmsghdr  nh;
		struct ifaddrmsg if_msg;
	}			req = {
		.nh	= {
			.nlmsg_len	= NLMSG_LENGTH(sizeof req.if_msg),
			.nlmsg_flags	= NLM_F_REQUEST | NLM_F_DUMP,
			.nlmsg_type	= RTM_GETADDR,
		},

		.if_msg	= {
			.ifa_family	= AF_INET6,
			.ifa_flags	= 0,
			.ifa_scope	= RT_SCOPE_LINK,
		},
	};
	ssize_t			l;

	l = send(fd, &req, req.nh.nlmsg_len, 0);
	if (l < 0) {
		fprintf(stderr, "send(<NETLINK>): %s\n", strerror(errno));
		return -1;
	}

	return 0;
}

static unsigned int	g_seq;

static struct ipchange_request *
create_request(struct ifaddrmsg const *msg, struct reqinfo const *info)
{
	/* TODO: zero cstamp/tstamp? */
	struct ifa_cacheinfo		cache_info = info->cache_info;
	uint32_t			flags = info->flags;

	struct rtattr			*rta;
	struct ipchange_request		*res;

	struct nlmsghdr			*nh;
	struct ifaddrmsg		*if_msg;
	size_t				len;
	size_t				msg_len;

	msg_len = NLMSG_LENGTH(sizeof *if_msg);
	msg_len = NLMSG_ALIGN(msg_len + RTA_LENGTH(sizeof info->addr));

	/* NOTE: we have to copy cache_info for CHG_RM_TMP too; when omitted
	 * it will be set to 'forever' */
	msg_len = NLMSG_ALIGN(msg_len + RTA_LENGTH(sizeof cache_info));

	switch (info->type) {
	case CHG_LT:
		cache_info = (struct ifa_cacheinfo) {
			.ifa_prefered	= 0,
			.ifa_valid	= 3600
		};
		break;

	case CHG_RM_TMP:
		msg_len = NLMSG_ALIGN(msg_len + RTA_LENGTH(sizeof flags));

		flags  &= ~IFA_F_MANAGETEMPADDR;
		break;

	default:
		return NULL;
	}

	/* zero allocated data; res->buf[] will be send to the kernel */
	res = calloc(1, sizeof *res + msg_len);
	if (!res)
		abort();

	res->seq = g_seq++;
	res->len = msg_len;

	nh  = (void *)res->buf;
	*nh = (struct nlmsghdr) {
		.nlmsg_len	= msg_len,
		.nlmsg_flags	= NLM_F_REQUEST | NLM_F_ACK | NLM_F_REPLACE,
		.nlmsg_type	= RTM_NEWADDR,
		.nlmsg_seq	= res->seq,
	};

	len    = NLMSG_PAYLOAD(nh, msg_len);
	if_msg = NLMSG_DATA(nh);
	*if_msg = *msg;

	/* copy IPv6 address */
	rta = IFA_RTA(if_msg);
	rta->rta_type = IFA_ADDRESS;
	rta->rta_len  = RTA_LENGTH(sizeof info->addr);
	memcpy(RTA_DATA(rta), &info->addr, sizeof info->addr);

	/* copy cache info; will be set to 'forever' when omitted */
	rta = RTA_NEXT(rta, len);
	rta->rta_type = IFA_CACHEINFO;
	rta->rta_len  = RTA_LENGTH(sizeof cache_info);
	memcpy(RTA_DATA(rta), &cache_info, sizeof cache_info);

	switch (info->type) {
	case CHG_LT:
		break;

	case CHG_RM_TMP:
		/* copy flags */
		rta = RTA_NEXT(rta, len);
		rta->rta_type = IFA_FLAGS,
		rta->rta_len  = RTA_LENGTH(sizeof flags);
		memcpy(RTA_DATA(rta), &flags, sizeof flags);
		break;
	}

	return res;
}

static bool filter_lt(struct reqinfo const *info)
{
	if ((info->flags & IFA_F_TEMPORARY) == 0)
		/* no temporary address */
		return false;

	if (info->addr.s6_addr[11] == 0xff && info->addr.s6_addr[12] == 0xfe) {
		/* no temporary address */
		return false;
	}

	if ((info->addr.s6_addr[0] & 0xfe)!= 0xfc)
		/* not site-local address */
		return false;

	if (info->cache_info.ifa_prefered == 0)
		/* already marked as deprecated */
		return false;

	return true;
}

static bool filter_rm_tmp(struct reqinfo const *info)
{
	if ((info->flags & IFA_F_MANAGETEMPADDR) == 0)
		/* no associated tempaddr */
		return false;

	if ((info->scope != RT_SCOPE_UNIVERSE))
		/* TODO: really ignore non-global addresses?  Indeed, we want
		 * to avoid temporary addresses on site-local addresses but
		 * these are marked as 'global' by linux */
		return false;

	if ((info->addr.s6_addr[0] & 0xfe) != 0xfc)
		/* none site-local address */
		return false;

	return true;
}

static int handle_newaddr(struct list_head *requests,
			  struct ifaddrmsg const *msg, size_t len)
{
	bool			have_addr = false;
	bool			have_cache_info = false;
	struct ipchange_request	*new_request;
	char			addr_buf[INET6_ADDRSTRLEN];
	struct reqinfo		info;

	if (len < sizeof *msg) {
		fprintf(stderr, "malformed ifaddrmsg\n");
		return -1;
	}

	if (msg->ifa_family != AF_INET6) {
		/* should not happen */
		fprintf(stderr, "no ipv6\n");
		return -1;
	}

	info = (struct reqinfo) {
		.type	= CHG_DFLT_MODE,
		.scope	= msg->ifa_scope,
		.flags	= msg->ifa_flags,
	};

	for (struct rtattr const *rta = IFA_RTA(msg); RTA_OK(rta, len);
	     rta = RTA_NEXT(rta, len)) {
		void const	*rta_data = RTA_DATA(rta);
		size_t		rta_len  = RTA_PAYLOAD(rta);

		switch (rta->rta_type) {
		case IFA_ADDRESS:
			if (rta_len < sizeof info.addr) {
				fprintf(stderr, "insufficient space for IFA_ADDRESS");
				return -1;
			}

			memcpy(&info.addr, rta_data, sizeof info.addr);
			have_addr = true;
			break;

		case IFA_CACHEINFO:
			if (rta_len < sizeof info.cache_info) {
				fprintf(stderr, "insufficient space for IFA_CACHEINFO");
				return -1;
			}

			memcpy(&info.cache_info, rta_data, sizeof info.cache_info);
			have_cache_info = true;
			break;

		case IFA_FLAGS:
			if (rta_len < sizeof info.flags) {
				fprintf(stderr, "insufficient space for IFA_FLAGS");
				return -1;
			}

			memcpy(&info.flags, rta_data, sizeof info.flags);
			break;
		}
	}

	if (!have_cache_info || !have_addr) {
		fprintf(stderr, "missing cache info or address\n");
		return 0;
	}

	inet_ntop(AF_INET6, &info.addr, addr_buf, sizeof addr_buf);
	if (0)
		printf("seeing ip %s with %04x\n", addr_buf, info.flags);

	switch (info.type) {
	case CHG_LT:
		if (!filter_lt(&info))
			return 0;

		break;

	case CHG_RM_TMP:
		if (!filter_rm_tmp(&info))
			return 0;
	}

	printf("changing ip %s with %04x\n", addr_buf, info.flags);

	new_request = create_request(msg, &info);
	if (!new_request)
		return -1;

	list_add_tail(&new_request->head, requests);
	return 0;
}

static int handle_error(struct list_head *pending,
			struct nlmsgerr const *msg, size_t len)
{
	struct ipchange_request		*tmp_req;
	struct ipchange_request		*req = NULL;

	if (len < sizeof *msg) {
		fprintf(stderr, "malformed nlmsgerr\n");
		return -1;
	}

	if (msg->msg.nlmsg_flags != NLM_F_ACK)
		return 0;

	list_foreach_entry(tmp_req, pending, head) {
		if (msg->msg.nlmsg_seq == tmp_req->seq) {
			req = tmp_req;
			break;
		}
	}

	if (!req) {
		fprintf(stderr, "unassociated error message\n");
		return 0;
	}

	list_del(&req->head);
	free(req);

	if (msg->error)
		fprintf(stderr, "failed to change ip: %s", strerror(-msg->error));

	return 0;
}

int main(void)
{
	struct sockaddr_nl	sa = {
		.nl_family	= AF_NETLINK,
		.nl_groups	= RTMGRP_IPV6_IFADDR,
	};
	int			fd;
	int			rc;
	bool			expect_done;
	struct list_head	requests = DECLARE_LIST(&requests);
	struct list_head	pending  = DECLARE_LIST(&pending);
	bool			is_startup = true;

	fd = socket(AF_NETLINK, SOCK_RAW | SOCK_CLOEXEC, NETLINK_ROUTE);
	if (fd < 0) {
		perror("socket(<AF_NETLINK>)");
		return EX_OSERR;
	}

	rc = bind(fd, (void *)&sa, sizeof sa);
	if (rc < 0) {
		perror("bind(<AF_NETLINK>)");
		close(fd);
		return EX_OSERR;
	}

	rc = request_netlink_info(fd);
	if (rc < 0) {
		return EX_OSERR;
	}

	expect_done = true;
	sd_notify(false, "STATUS=starting main loop");

	for (;;) {
		unsigned char		rcv_buf[1024 * 1024];
		ssize_t			l;

		while (!expect_done && !list_empty(&requests)) {
			struct ipchange_request	*req =
				list_first_entry(&requests,
						 struct ipchange_request,
						 head);

			list_del(&req->head);
			rc = send_request(fd, req);
			if (rc < 0)
				free(req);
			else
				list_add_tail(&req->head, &pending);
		}

		l = recv(fd, rcv_buf, sizeof rcv_buf, 0);
		if (l < 0) {
			perror("recv(<NETLINK>)");
			continue;
		}

		if (l == sizeof rcv_buf) {
			fprintf(stderr, "too much data\n");
			continue;
		}

		rc = 0;
		for (struct nlmsghdr *nh = (void *)rcv_buf;
		     NLMSG_OK(nh, l);
		     nh = NLMSG_NEXT(nh, l)) {
			void const	*nh_data = NLMSG_DATA(nh);
			size_t		nh_len = NLMSG_PAYLOAD(nh, l);

			switch (nh->nlmsg_type) {
			case NLMSG_DONE:
				expect_done = false;
				rc = 0;
				break;

			case NLMSG_ERROR:
				rc = handle_error(&pending, nh_data, nh_len);
				break;

			case RTM_NEWADDR:
				rc = handle_newaddr(&requests, nh_data, nh_len);
				break;

			default:
				rc = 0;
				/* TODO: print type? */
				break;
			}

			if (rc < 0)
				break;
		}

		if (rc < 0) {
			fprintf(stderr, "error while handling message\n");
			continue;
		}

		if (is_startup) {
			sd_notify(true, "READY=1");
			is_startup = false;
		}
	}
}
